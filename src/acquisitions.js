import Chart from 'chart.js/auto';

(async function() {
  const data = [
    { year: 2010, count: 10 },
    { year: 2011, count: 20 },
    { year: 2012, count: 15 },
    { year: 2013, count: 25 },
    { year: 2014, count: 22 },
    { year: 2015, count: 30 },
    { year: 2016, count: 28 },
  ];

  // Function to create and update the chart
  // Features: animated, a legend, grid lines, ticks, and tooltips shown on hover

  let chart = null; // Variable to store the chart instance

  // Function to create and update the chart
  function createChart() {
    if (chart) {
      chart.destroy(); // Destroy the existing chart instance
    }

    chart = new Chart(
      document.getElementById('acquisitions'), // Canvas element where the chart will be rendered
      {
        type: 'bar', // Chart type (bar chart)
        data: { // Each dataset is designated with a label and contains an array of data points.
          labels: data.map(row => row.year), // Array of labels (years)
          datasets: [
            {
              // label: 'Acquisitions by year',
              data: data.map(row => row.count) // Array of data values (counts)
            }
          ]
        },
        options: {
          responsive: true, // Enable responsiveness
          maintainAspectRatio: false, // Allow aspect ratio to be adjusted based on the container size
          animation: false, // Disables animation
          plugins: {
            legend: {
              display: false // Disables legend display
            },
            tooltip: {
              enabled: false // Disables tooltip
            }
          },
        }
      }
    );
  }

  // Call the createChart function initially
  createChart();

  // Add a resize event listener
  window.addEventListener('resize', createChart);
})();